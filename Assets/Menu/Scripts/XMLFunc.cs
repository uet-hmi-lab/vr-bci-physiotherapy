﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
namespace Xml
{
	[XmlRoot(ElementName="Shape")]
	public class Shape {
		[XmlElement(ElementName="Name")]
		public string Name { get; set; }
		[XmlElement(ElementName="Material")]
		public string Material { get; set; }
	}

	// [XmlRoot(ElementName="ArrayOfShape")]
	// public class ArrayOfShape {
	// 	[XmlElement(ElementName="Shape")]
	// 	public List<Shape> Shape { get; set; }
	// 	[XmlAttribute(AttributeName="xsi", Namespace="http://www.w3.org/2000/xmlns/")]
	// 	public string Xsi { get; set; }
	// 	[XmlAttribute(AttributeName="xsd", Namespace="http://www.w3.org/2000/xmlns/")]
	// 	public string Xsd { get; set; }
	// }

}