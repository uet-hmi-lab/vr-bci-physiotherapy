﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmAnimationController : MonoBehaviour
{
    public Animator armAni;
    
    static bool isAction = false;

    // Start is called before the first frame update
    void Start()
    {
        armAni = GetComponent<Animator>(); 
        Execute();
    }

    void Execute() 
    {
        if(Input.GetKeyDown("1") && !isAction) {
            armAni.Play("Left Hand Raise");
            // isAction = true;
            //textInput.Remove();
        }

        if(Input.GetKeyDown("2") && !isAction) {
            armAni.Play("Right Hand Raise");
            // isAction = false;
        }
    }

    // Update is called once per frame
    void Update()
    {   
        
        Execute();
    }
}
