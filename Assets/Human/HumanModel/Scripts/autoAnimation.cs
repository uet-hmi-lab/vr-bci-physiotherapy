﻿using System;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class autoAnimation : MonoBehaviour
{
    public Animator legAni;
    
    static bool isAction = false;
    static int count = 0;
    static int actionNumber = 0;
    string filePath = @"F:\IT\CSharpOut\OutputTesting\ActionInput.txt";
    List<string> testInput = new List<string>();

    // Start is called before the first frame update
    void Start()
    {
        legAni = GetComponent<Animator>(); 
        //Execute();
        StartCoroutine(ExecuteAfterTime());
    }

    public IEnumerator ExecuteAfterTime()
    {
        WaitForSeconds wait = new WaitForSeconds(5);
        
        for (int i = 0; i < 10; i++) 
        {
            yield return wait;
            Execute();
        }
        
    } 

    void Execute() 
    {
        if (File.Exists(filePath))
        {
            testInput = File.ReadAllLines(filePath).ToList();

            foreach (String line in testInput)
            {
                count += 1;
            }
            
            if (count != 0)
            {
                actionNumber = Convert.ToInt32(testInput[count-1]);
                if(actionNumber == 1 && !isAction) 
                {
                    legAni.Play("Raise Left Knee");
                    isAction = true;
                }

                if(actionNumber == 2 && isAction) 
                {
                    legAni.Play("Drop Left Knee");
                    isAction = false;
                }

                if(actionNumber == 3 && !isAction) 
                {
                    legAni.Play("Raise Right Knee");
                    isAction = true;
                }

                if(actionNumber == 4 && isAction) 
                {
                    legAni.Play("Drop Right Knee");
                    isAction = false;
                }

                if(actionNumber == 5 && !isAction) 
                {
                    legAni.Play("Raise Left Leg");
                    isAction = true;
                }

                if(actionNumber == 6 && isAction)
                {
                    legAni.Play("Drop Left Leg");
                    isAction = false;
                }

                if(actionNumber == 7 && !isAction) 
                {
                    legAni.Play("Raise Right Leg");
                    isAction = true;
                }

                if(actionNumber == 8 && isAction) {
                    legAni.Play("Drop Right Leg");
                    isAction = false;
                }
            }
            //Clear list
            //testInput.Clear();
            count = 0;
        }
    }

    // Update is called once per frame
    void Update()
    {   
        //Execute();
    }
}