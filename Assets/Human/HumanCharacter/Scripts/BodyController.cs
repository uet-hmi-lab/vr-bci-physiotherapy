﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyController : MonoBehaviour {

	public Transform camAnchor;
	public Transform cam;

	private Animator animator;
	private Transform head;
	void Start () {
		animator = GetComponent<Animator> ();
		head = animator.GetBoneTransform (HumanBodyBones.Head);
	}
	
	void LateUpdate () {
		head.rotation = cam.rotation;
		cam.position = camAnchor.position;
	}
}
