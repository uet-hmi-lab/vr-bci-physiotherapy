﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CeilingColorChange : MonoBehaviour
{
    public string materialName;
    public MeshRenderer mesh;
    private Button btn;
    void Start()
    {
        mesh = GameObject.FindGameObjectWithTag("Roof").GetComponent<MeshRenderer>();
        //btn = GetComponent<Button>();
        //btn.onClick.AddListener(ChangeColor);
    }
    public void ChangeColor(){
        UnityEngine.Debug.Log("in");
        mesh.material = Resources.Load<Material>("Room Elements/Ceil/Materials/" + materialName);
    }
    void Update(){
        if(Input.GetKeyDown(KeyCode.Space)){
            ChangeColor();
        }
    }
    
}
