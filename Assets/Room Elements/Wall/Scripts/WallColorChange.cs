﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WallColorChange : MonoBehaviour
{
    public string materialName;
    private GameObject[] wallObjects;
    public MeshRenderer mesh;
    private Button btn;
    void Start()
    {
        wallObjects = GameObject.FindGameObjectsWithTag("Wall");
    }
    public void ChangeColor(){
        UnityEngine.Debug.Log("in");
        foreach(GameObject tempObject in wallObjects)
        {
            MeshRenderer meshRenderer = tempObject.GetComponent<MeshRenderer>();
            meshRenderer.material = Resources.Load<Material>("Room Elements/Wall/Materials/" + materialName);
        }
    }
    void Update(){
        if(Input.GetKeyDown(KeyCode.Space)){
            ChangeColor();
        }
    }
    
}
