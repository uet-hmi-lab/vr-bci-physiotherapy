﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloorColorChange : MonoBehaviour
{
    public string materialName;
    public MeshRenderer mesh;
    private Button btn;
    void Start()
    {
        mesh = GameObject.FindGameObjectWithTag("Floor").GetComponent<MeshRenderer>();
        //btn = GetComponent<Button>();
        //btn.onClick.AddListener(ChangeColor);
    }
    public void ChangeColor(){
        UnityEngine.Debug.Log("in");
        mesh.material = Resources.Load<Material>("Room Elements/Floor/Materials/" + materialName);
    }
    void Update(){
        
    }
    
}
